"""Webhook interaction tests."""
import copy
import unittest
from unittest import mock

from cki_lib import session

import webhook.common
import webhook.defs
import webhook.fixes


@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
@mock.patch('webhook.common.is_event_target_branch_active', mock.Mock(return_value=True))
class TestFixes(unittest.TestCase):
    """ Test Webhook class."""

    PAYLOAD_MERGE = {'object_kind': 'merge_request',
                     'project': {'id': 1,
                                 'archived': False,
                                 'web_url': 'https://web.url/g/p',
                                 'path_with_namespace': 'g/p'
                                 },
                     'object_attributes': {'target_branch': 'main', 'iid': 2,
                                           'url': 'https://web.url/g/p/-/merge_requests/2'},
                     'changes': {'labels': {'previous': [],
                                            'current': []}},
                     'description': 'dummy description',
                     'state': 'opened',
                     'user': {'username': 'test_user'}
                     }

    MOCK_PROJS = {'projects': [{'name': 'project1',
                                'id': 12345,
                                'group_id': 9876,
                                'inactive': False,
                                'product': 'Project 1',
                                'webhooks': {'bughook': {'name': 'bughook'}},
                                'branches': [{'name': 'main',
                                              'inactive': False,
                                              'component': 'kernel',
                                              'distgit_ref': 'rhel-10.1.0',
                                              'internal_target_release': '10.1.0'
                                              },
                                             {'name': '10.1',
                                              'component': 'kernel',
                                              'distgit_ref': 'rhel-10.1.0',
                                              'internal_target_release': '10.1.0'
                                              },
                                             {'name': '10.0',
                                              'component': 'kernel',
                                              'distgit_ref': 'rhel-10.0.0',
                                              'zstream_target_release': '10.0.0'
                                              }]
                                }]
                  }

    @mock.patch('gitlab.Gitlab')
    def test_unsupported_object_kind(self, mocked_gitlab):
        """Check handling an unsupported object kind."""
        payload = copy.deepcopy(self.PAYLOAD_MERGE)
        payload.update({'object_kind': 'foo'})

        self._test_payload(mocked_gitlab, False, payload=payload)

    @mock.patch.dict('os.environ', {'RH_METADATA_YAML_PATH': 'tests/fake_rh_metadata.yaml'})
    @mock.patch('webhook.fixes.FixesMR.find_potential_missing_fixes')
    @mock.patch('webhook.common.extract_dependencies')
    @mock.patch('webhook.cdlib.get_dependencies_data')
    @mock.patch('gitlab.Gitlab')
    def test_merge_request(self, mocked_gitlab, dep_data, ext_deps, fpmf):
        """Check handling of a merge request."""
        payload = copy.deepcopy(self.PAYLOAD_MERGE)
        payload['object_attributes']['target_branch'] = 'os-build'
        payload['object_attributes']['action'] = 'open'
        dep_data.return_value = (False, 'abcd')
        ext_deps.return_value = []
        self._test_payload(mocked_gitlab, True, payload=payload)

    def test_find_intentionally_omitted_fixes(self):
        """Check the results we get from the tested function."""
        fixes_mr = mock.Mock()
        fixes_mr.omitted = set()
        description = "Omitted-fix: abcdef012345"
        webhook.fixes.FixesMR.find_intentionally_omitted_fixes(fixes_mr, description)
        self.assertEqual({'abcdef012345'}, fixes_mr.omitted)

    @mock.patch('git.Repo')
    def test_map_fixes_to_commits(self, mocked_repo):
        """Make sure we're mapping RH-Fixes entries correctly."""
        mocked_repo.git.log.return_value = ("commit aaaabbbbccccdddd\n"
                                            "Author: someone\n\n"
                                            "title\n")
        fixes_mr = mock.Mock()
        fixes_mr.fixes = {'1234abcd1234': 'aaaabbbbccccdddd'}
        rhcommit = mock.Mock()
        rhcommit.fixes = ""
        commit = mock.Mock(id='deadbeef1234', title="this is a fix")
        rhcommit.commit = commit
        rhcommit.ucids = ['1234abcd1234']
        fixes_mr.rhcommits = {'deadbeef1234': rhcommit}
        webhook.fixes.FixesMR.map_fixes_to_commits(fixes_mr, mocked_repo)
        self.assertIn("commit aaaabbbbccccdddd", rhcommit.fixes)
        self.assertIn("RH-Fixes: deadbeef1234 (\"this is a fix\")", rhcommit.fixes)

    def test_filter_fixes(self):
        """Make sure we're filtering out existing and omitted fixes correctly."""
        fixes_mr = mock.Mock()
        fixes_mr.kernel_src = "/usr/src/linux"
        fixes_mr.ucids = {'abcdef012345', '112233445566', 'deadbeef1234'}
        fixes_mr.fixes = {}
        fixes_mr.rhcommits = {}
        fixes_mr.omitted = {'8675309abcde'}
        mapped_fixes = {'abcdef012345': ['112233445566', '8675309abcde'],
                        '112233445566': ['112233445566', '8675309abcde'],
                        'deadbeef1234': ['asdf8675309e']}
        with self.assertLogs('cki.webhook.fixes', level='DEBUG') as logs:
            webhook.fixes.FixesMR.filter_fixes(fixes_mr, mapped_fixes)
            self.assertEqual({'deadbeef1234': ['asdf8675309e']}, fixes_mr.fixes)
            self.assertIn("Found 112233445566 in ucids", ' '.join(logs.output))
            self.assertIn("Found 8675309abcde in omitted", ' '.join(logs.output))

    @mock.patch('git.Repo')
    def test_find_potential_missing_fixes(self, mocked_repo):
        """Check the results we get from the tested function."""
        mocked_repo().git.log.return_value = ('Fixes: 112233445566 fix one\n'
                                              'Fixes: 8675309abcde fix two\n')
        fixes_mr = mock.Mock()
        fixes_mr.kernel_src = "/usr/src/linux"
        fixes_mr.ucids = {'abcdef012345', '112233445566'}
        fixes_mr.fixes = {}
        fixes_mr.rhcommits = {}
        fixes_mr.omitted = {'8675309abcde'}
        with self.assertLogs('cki.webhook.fixes', level='DEBUG') as logs:
            webhook.fixes.FixesMR.find_potential_missing_fixes(fixes_mr)
            self.assertEqual({}, fixes_mr.fixes)
            self.assertIn("Commits with Fixes: ['112233445566']", ' '.join(logs.output))
            self.assertIn("Map of possible Fixes: {}", ' '.join(logs.output))

    @mock.patch('webhook.fixes.FixesMR.extract_ucid', mock.Mock())
    def test_find_upstream_commit_ids(self):
        """Make sure we can find upstream commit IDs."""
        fixes_mr = mock.Mock()
        rhcommit = mock.Mock()
        commit = mock.Mock(id='deadbeef1234', title="this is a fix")
        rhcommit.commit = commit
        rhcommit.ucids = ['1234abcd1234']
        fixes_mr.rhcommits = {'deadbeef1234': rhcommit}
        d1 = mock.Mock(text="1\ncommit 1234567890abcdef1234567890abcdef12345678")
        c1 = mock.Mock(id="1234", author_email="jdoe@redhat.com",
                       title="This is commit 1", parent_ids=['abcd'], description=d1)
        fixes_mr.commits = {'abc123abc123': c1}
        fixes_mr.ucids = [commit.id]
        fixes_mr.extract_ucid.return_value = ['1234abcd1234']

        with self.assertLogs('cki.webhook.fixes', level='DEBUG') as logs:
            webhook.fixes.FixesMR.find_upstream_commit_ids(fixes_mr)
            self.assertIn("List of 1 ucids: ['deadbeef1234']", ' '.join(logs.output))

    def test_build_fixes_comment(self):
        """Make sure we get back a sane looking fixes message."""
        fixes_mr = mock.Mock()
        fixes_mr.iid = '666'
        rhcommit = mock.Mock()
        rhcommit.fixes = "commit abcd1234\n"
        fixes_mr.rhcommits = {'deadbeef1234': rhcommit}

        fixes_mr.fixes = []
        ret = webhook.fixes.build_fixes_comment(fixes_mr)
        self.assertEqual(ret, "No missing upstream fixes for MR 666 found at this time.\n")
        fixes_mr.fixes = ['abcd1234']
        ret = webhook.fixes.build_fixes_comment(fixes_mr)
        expected = "\nPossible missing Fixes detected upstream:  \n```\ncommit abcd1234\n```\n"
        self.assertEqual(ret, expected)

    @mock.patch('webhook.fixes.get_fixes_mr')
    @mock.patch('webhook.fixes.report_results')
    def test_process_mr_no_label_changes(self, mock_get_fixes_mr, mock_rr):
        msg = mock.Mock()
        graphql = mock.Mock()
        projects = mock.Mock()
        msg.payload = {'object_kind': 'merge_request',
                       'project': {'id': 1,
                                   'web_url': 'https://web.url/g/p'},
                       'object_attributes': {'iid': 2,
                                             'action': 'update'},
                       'changes': {'labels': {'previous': [{'title': 'CommitRefs::OK'}],
                                              'current': [{'title': 'CommitRefs::OK'}]}}
                       }
        webhook.fixes.process_mr(None, msg, graphql, projects)
        mock_get_fixes_mr.assert_not_called()
        mock_rr.assert_not_called()

    def test_extract_ucid(self):
        fixes_mr = mock.Mock()
        fixes_mr.ucids = set()
        fixes_mr.omitted = set()
        d1 = mock.Mock(text="1\ncommit 1234567890abcdef1234567890abcdef12345678")
        c1 = mock.Mock(id="1234", author_email="jdoe@redhat.com",
                       title="This is commit 1",
                       description=d1,
                       parent_ids=["abcd"])
        d2 = mock.Mock(text="2\ncommit a012345")
        c2 = mock.Mock(id="4567", author_email="xyz@example.com",
                       title="This is commit 2",
                       description=d2,
                       parent_ids=["1234"])
        d7 = mock.Mock(text="6\n (cherry picked from commit 1234567890abcdef)")
        c7 = mock.Mock(id="9845", author_email="developer@redhat.com",
                       title="Merge: This is commit 7",
                       description=d7,
                       short_id="abc3efg2", parent_ids=["1357", "2468"])
        d8 = mock.Mock(text="1\n(cherry picked from commit "
                            "1234567890abcdef1234567890abcdef12345678)"
                            "\n (cherry picked from commit "
                            "2234567890abcdef1234567890abcdef12345678)"
                            "\n(cherry picked from commit   "
                            "3234567890abcdef1234567890abcdef12345678)")
        c8 = mock.Mock(id="1234", author_email="jdoe@redhat.com",
                       title="This is commit 8",
                       description=d8,
                       parent_ids=["abcd"])
        d9 = mock.Mock(text="1\ncommit 1234567890abcdef1234567890abcdef12345678"
                            "\ncommit 2234567890abcdef1234567890abcdef12345678"
                            "\n commit 3234567890abcdef1234567890abcdef12345678"
                            "\ncommit 4234567890abcdef1234567890abcdef12345678"
                            "\ncommit  5234567890abcdef1234567890abcdef12345678")
        c9 = mock.Mock(id="1234", author_email="jdoe@redhat.com",
                       title="This is commit 8",
                       description=d9,
                       parent_ids=["abcd"])
        d10 = mock.Mock(text="XYZ\nUpstream Status: https://github.com/torvalds/linux.git")
        c10 = mock.Mock(id="1234567890", author_email="jdoe@redhat.com",
                        title="This is a commit",
                        description=d10,
                        parent_ids=["abcdef0123"])
        self.assertEqual(webhook.fixes.FixesMR.extract_ucid(fixes_mr, c1),
                         ["1234567890abcdef1234567890abcdef12345678"])
        self.assertEqual(webhook.fixes.FixesMR.extract_ucid(fixes_mr, c2), [])
        fixes_mr.project.commits.get.return_value = c7
        self.assertEqual(webhook.fixes.FixesMR.extract_ucid(fixes_mr, c7), [])
        self.assertEqual(webhook.fixes.FixesMR.extract_ucid(fixes_mr, c8),
                         ["1234567890abcdef1234567890abcdef12345678"])
        self.assertEqual(webhook.fixes.FixesMR.extract_ucid(fixes_mr, c9),
                         ["1234567890abcdef1234567890abcdef12345678",
                          "2234567890abcdef1234567890abcdef12345678",
                          "4234567890abcdef1234567890abcdef12345678"])
        # This commit should NOT be considered Posted, as Linus' tree always needs a hash
        self.assertEqual(webhook.fixes.FixesMR.extract_ucid(fixes_mr, c10), [])

    def _test_payload(self, mocked_gitlab, result, payload):
        with mock.patch('cki_lib.misc.is_production', return_value=True):
            self._test(mocked_gitlab, result, payload, [])
        with mock.patch('cki_lib.misc.is_production', return_value=False):
            self._test(mocked_gitlab, result, payload, [])

    # pylint: disable=too-many-arguments
    @mock.patch('webhook.base_mr_mixins.GraphMixin.query', mock.Mock())
    @mock.patch('webhook.common.update_webhook_comment', mock.Mock())
    @mock.patch('webhook.common.add_label_to_merge_request', mock.Mock())
    def _test(self, mocked_gitlab, result, payload, labels, assert_labels=None):

        # setup dummy gitlab data
        project = mock.Mock(name_with_namespace="foo.bar",
                            namespace={'name': '8.y'})
        target = "main"
        if payload['object_kind'] == 'merge_request':
            target = payload['object_attributes']['target_branch']
        elif payload['object_kind'] == 'note':
            target = payload['merge_request']['target_branch']
        else:
            return
        merge_request = mock.Mock(target_branch=target)
        merge_request.author = {'id': 1, 'username': 'jdoe'}
        merge_request.iid = 2
        merge_request.state = payload["state"]
        merge_request.description = payload["description"]
        d1 = mock.Mock(text="1\ncommit 1234567890abcdef1234567890abcdef12345678")
        c1 = mock.Mock(id="1234", author_email="jdoe@redhat.com",
                       title="This is commit 1", parent_ids=['abcd'],
                       description=d1)
        c1.diff = mock.Mock(return_value=[{'new_path': 'include/linux/netdevice.h'}])
        d2 = mock.Mock(text="2\ncommit a01234567890abcdef1234567890abcdef123456")
        c2 = mock.Mock(id="4567", author_email="xyz@example.com",
                       title="This is commit 2", parent_ids=['abce'],
                       description=d2)
        c2.diff = mock.Mock(return_value=[{'new_path': 'include/linux/netdevice.h'}])
        d3 = mock.Mock(text="3\nUpstream Status: RHEL-only")
        c3 = mock.Mock(id="890a", author_email="jdoe@redhat.com",
                       title="This is commit 3", parent_ids=['abcf'],
                       description=d3)
        c3.diff = mock.Mock(return_value=[{'new_path': 'include/linux/netdevice.h'}])
        d4 = mock.Mock(text="4\n"
                       "(cherry picked from commit abcdef0123456789abcdef0123456789abcdef01)")
        c4 = mock.Mock(id="deadbeef", author_email="zzzzzz@redhat.com",
                       description=d4,
                       title="This is commit 4", parent_ids=['abc0'])
        c4.diff = mock.Mock(return_value=[{'new_path': 'include/linux/netdevice.h'}])
        d5 = mock.Mock(text="5\nUpstream-status: Posted")
        c5 = mock.Mock(id="0ff0ff00", author_email="spam@redhat.com",
                       title="This is commit 5", parent_ids=['abc1'],
                       description=d5)
        c5.diff = mock.Mock(return_value=[{'new_path': 'include/linux/netdevice.h'}])
        d6 = mock.Mock(text="6\nUpstream-status: Embargoed")
        c6 = mock.Mock(id="f00df00d", author_email="spam@redhat.com",
                       title="This is commit 6", parent_ids=['abc2'],
                       description=d6)
        c6.diff = mock.Mock(return_value=[{'new_path': 'include/linux/netdevice.h'}])
        mocked_gitlab().__enter__().projects.get.return_value = project
        project.mergerequests.get.return_value = merge_request
        project.labels.list.return_value = []
        project.namespace = mock.MagicMock(id=1, full_path=webhook.defs.RHEL_KERNEL_NAMESPACE)
        project.archived = payload["project"]["archived"]
        merge_request.labels = labels
        merge_request.commits.return_value = [c1, c2, c3, c4, c5, c6]
        merge_request.pipeline = {'status': 'success'}
        gl_instance = mock.Mock()
        gl_instance.users.get.return_value = mock.Mock()
        rh_group = mock.Mock(id=10810393)
        gl_instance.groups.list.return_value = [rh_group]
        branch = mock.Mock()
        branch.configure_mock(name="8.2")
        project.branches.list.return_value = [branch]
        project.commits = {"1234": c1, "4567": c2, "890a": c3, "deadbeef": c4, "00f00f00": c5,
                           "f00df00d": c6}
        fixes_mr = mock.Mock()
        fixes_mr.ucids = set()
        fixes_mr.omitted = set()
        self.assertEqual(webhook.fixes.FixesMR.extract_ucid(fixes_mr, c1),
                         ["1234567890abcdef1234567890abcdef12345678"])
        self.assertEqual(webhook.fixes.FixesMR.extract_ucid(fixes_mr, c2),
                         ["a01234567890abcdef1234567890abcdef123456"])
        self.assertEqual(webhook.fixes.FixesMR.extract_ucid(fixes_mr, c3), [])
        self.assertEqual(webhook.fixes.FixesMR.extract_ucid(fixes_mr, c4),
                         ["abcdef0123456789abcdef0123456789abcdef01"])
        self.assertEqual(webhook.fixes.FixesMR.extract_ucid(fixes_mr, c5), [])
        self.assertEqual(webhook.fixes.FixesMR.extract_ucid(fixes_mr, c6), [])

        # setup dummy post results data
        presult = mock.Mock()
        presult.json.return_value = {
            "error": "*** No upstream commit ID found in submitted patch!\n***",
            "result": "fail",
            "logs": "*** Comparing submitted patch with upstream commit ID\n***"
        }

        headers = {'message-type': 'gitlab'}
        args = webhook.common.get_arg_parser('FIXES').parse_args('')
        args.disable_user_check = True
        with mock.patch('cki_lib.session.get_session') as mock_session:
            mock_session.side_effect = session.get_session()
            mock_session.post.return_value = presult
            return_value = \
                webhook.common.process_message(args, webhook.fixes.WEBHOOKS, 'dummy',
                                               payload, headers, linux_src='/src/linux',
                                               get_graphql_instance=True)

        if result:
            mocked_gitlab.assert_called()
            mocked_gitlab().__enter__().projects.get.assert_called_with('g/p')
            project.mergerequests.get.assert_called_with(2)
            self.assertTrue(return_value)
            if assert_labels:
                self.assertEqual(sorted(merge_request.labels), sorted(assert_labels))
        else:
            self.assertFalse(return_value)
