"""Tests for the defs."""
from unittest import TestCase

from webhook import defs


class TestBZStatus(TestCase):
    """Tests for the BZStatus enum."""

    def test_from_str(self):
        """Returns the BZStatus member whose name matches the input string, or UNKNOWN."""
        self.assertEqual(len(defs.BZStatus), 10)
        self.assertIs(defs.BZStatus.from_str('New'), defs.BZStatus.NEW)
        self.assertIs(defs.BZStatus.from_str('POST'), defs.BZStatus.POST)
        self.assertIs(defs.BZStatus.from_str('modified'), defs.BZStatus.MODIFIED)
        self.assertIs(defs.BZStatus.from_str('crazy'), defs.BZStatus.UNKNOWN)


class TestMrScope(TestCase):
    """Tests for the MrScope enum."""

    def test_mrscope_label(self):
        """Returns a label string."""
        prefix = 'Bugzilla'
        self.assertEqual(defs.MrScope.INVALID.label(prefix), f'{prefix}::Invalid')
        self.assertEqual(defs.MrScope.NEEDS_REVIEW.label(prefix),
                         f'{prefix}::{defs.NEEDS_REVIEW_SUFFIX}')
        self.assertEqual(defs.MrScope.READY_FOR_QA.label(prefix),
                         f'{prefix}::{defs.NEEDS_TESTING_SUFFIX}')
        self.assertEqual(defs.MrScope.READY_FOR_MERGE.label(prefix),
                         f'{prefix}::{defs.READY_SUFFIX}')
        self.assertEqual(defs.MrScope.CLOSED.label(prefix), f'{prefix}::Closed')
        self.assertEqual(defs.MrScope.FAILED.label(prefix), f'{prefix}::Failed')
        self.assertEqual(defs.MrScope.TESTING_FAILED.label(prefix),
                         f'{prefix}::{defs.TESTING_FAILED_SUFFIX}')
        self.assertEqual(defs.MrScope.WAIVED.label(prefix),
                         f'{prefix}::{defs.TESTING_WAIVED_SUFFIX}')

    def test_mrscope_get(self):
        """Returns the MrScope with the matching name."""
        names = ['invalid', 'closed', 'needs_review', 'ready_for_qa', 'ready_for_merge', 'merged',
                 'ok']
        for name in names:
            self.assertIs(defs.MrScope.get(name), getattr(defs.MrScope, name.upper()))


class TestMrState(TestCase):
    """Tests for the MrState enum."""

    def test_mrstate_from_str(self):
        """Returns the MrState member whose name matches the given string."""
        self.assertIs(defs.MrState.from_str('closed'), defs.MrState.CLOSED)
        self.assertIs(defs.MrState.from_str('Locked'), defs.MrState.LOCKED)
        self.assertIs(defs.MrState.from_str('MERGED'), defs.MrState.MERGED)
        self.assertIs(defs.MrState.from_str('opened'), defs.MrState.OPENED)
        self.assertIs(defs.MrState.from_str('chicken'), defs.MrState.UNKNOWN)


class TestDCOState(TestCase):
    """Tests for the DCOState enum."""

    def test_dcostate(self):
        """Returns the right footnote and formatted title."""
        for state in defs.DCOState:
            self.assertEqual(state.footnote, defs.DCO_FOOTNOTES[state])
            title = 'OK' if state is defs.DCOState.OK else state.name.replace('_', ' ').capitalize()
            self.assertEqual(state.title, title)


class TestLabel(TestCase):
    """Tests for the Label object."""

    def test_label(self):
        """Returns valid values."""
        label_str = defs.Label('Acks::NeedsReview')
        self.assertEqual(label_str.gl_prefix, 'Acks')
        self.assertEqual(label_str.prefix, 'Acks')
        self.assertEqual(label_str.scope, defs.MrScope.NEEDS_REVIEW)
        self.assertEqual(label_str.scoped, 1)
        self.assertEqual(label_str.primary, 'NeedsReview')
        self.assertEqual(label_str.secondary, None)

        label_str = defs.Label('ExternalCI::lnst::OK')
        self.assertEqual(label_str.gl_prefix, 'ExternalCI::lnst')
        self.assertEqual(label_str.prefix, 'ExternalCI')
        self.assertEqual(label_str.scope, defs.MrScope.OK)
        self.assertEqual(label_str.scoped, 2)
        self.assertEqual(label_str.primary, 'lnst')
        self.assertEqual(label_str.secondary, 'OK')

        label_str = defs.Label('readyForMerge')
        self.assertEqual(label_str.gl_prefix, 'readyForMerge')
        self.assertEqual(label_str.prefix, 'readyForMerge')
        self.assertEqual(label_str.scope, None)
        self.assertEqual(label_str.scoped, 0)
        self.assertEqual(label_str.primary, None)
        self.assertEqual(label_str.secondary, None)

        label_str = defs.Label('Subsystem:networking')
        self.assertEqual(label_str.gl_prefix, 'Subsystem:networking')
        self.assertEqual(label_str.prefix, 'Subsystem:networking')
        self.assertEqual(label_str.scope, None)
        self.assertEqual(label_str.scoped, 0)
        self.assertEqual(label_str.primary, None)
        self.assertEqual(label_str.secondary, None)

        # Don't even try more than two :: pairs.
        with self.assertRaises(ValueError):
            defs.Label('readyForMerge::really::now::go')
