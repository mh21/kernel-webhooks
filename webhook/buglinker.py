"""Update bugs to contain links to an MR and successful pipeline artifacts."""
import json
from os import environ
import re
import sys
from xmlrpc.client import Fault

from bugzilla import BugzillaError
from cki_lib import logger
from cki_lib import misc
from cki_lib.gitlab import get_instance
from cki_lib.gitlab import get_variables
from cki_lib.gitlab import parse_gitlab_url
from gitlab import GitlabGetError

from . import common
from . import defs
from .description import Description
from .jissue import make_jissues
from .libjira import remove_gitlab_link_in_issues
from .libjira import update_testable_builds
from .pipelines import PipelineType

LOGGER = logger.get_logger('cki.webhook.buglinker')

HEADER = ('The following Merge Request has pipeline job artifacts available:\n\n'
          'Title: %s\nMR: %s\n'
          'MR Pipeline: %s\n\n'
          'The Repo URLs are *not* accessible from a web browser! '
          'They only function as a dnf or yum baseurl.\n\n'
          'Artifacts expire six weeks after creation. If artifacts are needed after that '
          "time please rerun the pipeline by visiting the MR's 'Pipelines' tab and clicking "
          "the 'Run pipeline' button.\n\n")


def get_bugs(bzcon, bug_list):
    """Return a list of bug objects."""
    try:
        bz_results = bzcon.getbugs(bug_list)
        if not bz_results:
            LOGGER.info("getbugs() returned an empty list for these bugs: %s.", bug_list)
        return bz_results
    except BugzillaError:
        LOGGER.exception('Error getting bugs.')
        return None


def make_ext_bz_bug_id(namespace, mr_id):
    """Format the string needed for the tracker links."""
    return f"{namespace}/-/merge_requests/{mr_id}"


def update_bugzilla(bugs, namespace, mr_id, bzcon):
    """Filter input bug list and run bugzilla API actions."""
    ext_bz_bug_id = make_ext_bz_bug_id(namespace, mr_id)
    for action, bug_list in bugs.items():
        if not bug_list or action not in ('link', 'unlink'):
            continue
        if action == 'unlink':
            unlink_mr_from_bzs(bug_list, ext_bz_bug_id, bzcon)
        else:
            bugs = get_bugs(bzcon, bug_list)
            link_mr_to_bzs(bugs, ext_bz_bug_id, bzcon)


def bz_is_linked_to_mr(bug, ext_bz_bug_id):
    """Return the matching external tracker for the BZ, if any."""
    return next((tracker for tracker in bug.external_bugs if
                 tracker['type']['description'] == 'Gitlab' and
                 tracker['type']['url'] == defs.EXT_TYPE_URL and
                 tracker['ext_bz_bug_id'] == ext_bz_bug_id),
                None)


def unlink_mr_from_bzs(bugs, ext_bz_bug_id, bzcon):
    """Unlink the MR from the given list of BZs."""
    # Add any related kernel-rt bugs
    rt_bugs = {bug.id for bug in get_rt_cve_bugs(bzcon, bugs)}
    for bug in set(bugs).union(rt_bugs):
        LOGGER.info('Unlinking %s from BZ%s.', ext_bz_bug_id, bug)
        if not misc.is_production():
            continue
        try:
            bzcon.remove_external_tracker(ext_type_description='Gitlab',
                                          ext_bz_bug_id=ext_bz_bug_id, bug_ids=bug)
        except BugzillaError:
            LOGGER.exception("Problem unlinking %s from BZ%s.", ext_bz_bug_id, bug)
        except Fault as err:
            if err.faultCode == 1006:
                LOGGER.warning('xmlrpc fault %d: %s', err.faultCode, err.faultString)
            else:
                raise


def get_kernel_bugs(bugs):
    """Filter out bugs which are not RHEL & a kernel component."""
    return [bug for bug in bugs if
            bug.product.startswith('Red Hat Enterprise Linux') and
            bug.component in ('kernel', 'kernel-rt')]


def _do_link_mr_to_bzs(bugs, ext_bz_bug_id, bzcon):
    if misc.is_production():
        try:
            bzcon.add_external_tracker(bugs, ext_type_url=defs.EXT_TYPE_URL,
                                       ext_bz_bug_id=ext_bz_bug_id)
        except BugzillaError:
            LOGGER.exception("Problem adding tracker %s to BZs.", ext_bz_bug_id)


def link_mr_to_bzs(bugs, ext_bz_bug_id, bzcon):
    """Take a list of bugs and a MR# and update the BZ's ET with the MR url."""
    untracked_bugs = [bug for bug in bugs if not bz_is_linked_to_mr(bug, ext_bz_bug_id)]
    if not untracked_bugs:
        LOGGER.info("All bugs have an existing link to %s.", ext_bz_bug_id)
        return

    kernel_bugs = get_kernel_bugs(untracked_bugs)
    bug_list = [bug.id for bug in kernel_bugs]

    LOGGER.info("Need to add %s to external tracker list of these bugs: %s", ext_bz_bug_id,
                bug_list)
    _do_link_mr_to_bzs(bug_list, ext_bz_bug_id, bzcon)


BASIC_BZ_FIELDS = ['component',
                   'external_bugs',
                   'id',
                   'product',
                   'sub_component'
                   'summary'
                   ]

BASIC_BZ_QUERY = {'query_format': 'advanced',
                  'include_fields': BASIC_BZ_FIELDS,
                  'classification': 'Red Hat',
                  'component': 'kernel-rt'
                  }


def parse_cve_from_summary(summary):
    """Return a CVE ID from the string, or None."""
    result = re.match(r'^(\w* )?(?P<cve>CVE-\d{4}-\d{4,7})\s', summary)
    if result:
        return result.group('cve')
    return None


def get_rt_cve_bugs(bzcon, bug_list):
    """Identify whether bug_list BZs are CVEs and if so return matching kernel-rt bugs."""
    # Filter out any kernel-rt bugs given in the bug_list.
    bz_results = [bug for bug in get_bugs(bzcon, bug_list) if bug.component != 'kernel-rt']
    cve_set = set()
    for bug in bz_results:
        cve_id = parse_cve_from_summary(bug.summary)
        if not cve_id:
            continue
        cve_set.add(cve_id)

    if not cve_set:
        return []

    cve_query = {**BASIC_BZ_QUERY}
    cve_query['product'] = bz_results[0].product
    # Blocked by the CVE
    cve_query['f1'] = 'blocked'
    cve_query['o1'] = 'anywords'
    cve_query['v1'] = list(cve_set)
    # Is not CLOSED DUPLICATE.
    cve_query['f2'] = 'resolution'
    cve_query['o2'] = 'notequals'
    cve_query['v2'] = 'DUPLICATE'
    # Is the same version.
    cve_query['f3'] = 'version'
    cve_query['o3'] = 'equals'
    cve_query['v3'] = bz_results[0].version

    try:
        return bzcon.query(cve_query)
    except BugzillaError:
        LOGGER.exception('Error querying bugzilla.')
        return []


def post_to_bugs(bug_list, post_text, pipeline_url, pipe_type, bzcon):
    # pylint: disable=too-many-branches
    """Submit post to given bugs."""
    LOGGER.info('Posting results for %s. bug_list is: %s', pipe_type.name, bug_list)
    filtered_bugs = []
    if not (bugs := get_bugs(bzcon, bug_list)):
        LOGGER.warning('No bug data found.')
        return None
    # If this is the RT pipeline and a CVE then we want to post to the kernel-rt bug.
    if pipe_type is PipelineType.REALTIME:
        if rt_bugs := get_rt_cve_bugs(bzcon, bug_list):
            LOGGER.info('Including CVE bugs %s', rt_bugs)
            bugs.extend(rt_bugs)
        else:
            LOGGER.info('No RT CVE bug data found.')

    # If this is an automotive pipeline then only post if the BZs' component is kernel-automotive.
    if pipe_type is PipelineType.AUTOMOTIVE:
        if not (bugs := [bug for bug in bugs if bug.component == 'kernel-automotive']):
            LOGGER.info("Ignoring automotive pipeline as no bug has 'automotive' subcomponent")
            return None

    # It is possible for there to be multiple 'success' events for a given pipeline
    # so we should check that we haven't already posted them to the bugs. Sigh.
    for bug in bugs:
        if bug.id not in filtered_bugs and not comment_already_posted(bug, pipeline_url):
            filtered_bugs.append(bug.id)
    if not filtered_bugs:
        LOGGER.info('This pipeline has already been posted to all relevant bugs.')
        return None

    LOGGER.info('Updating these bugs %s with comment:\n%s', filtered_bugs, post_text)
    if misc.is_production():
        # Adding a comment with extra_private_groups is not possible with
        # python-bugzilla (it misses support for add_comment, you can only
        # update existing comments). Thus call the api directly using
        # JSON RPC.
        data = {
            "method": "Bug.add_comment",
            "version": "2.0",
            "params": {
                "id": 0,
                "comment": post_text,
                "is_private": 1,
                "minor_update": 0,
                "extra_private_groups": ["redhat_partner_engineers"]
            },
        }
        bz_url = bzcon.url.replace('xmlrpc.cgi', 'jsonrpc.cgi')
        sess = bzcon.get_requests_session()
        for bug in filtered_bugs:
            data["params"]["id"] = bug
            res = sess.post(url=bz_url, json=data,
                            headers={'Content-Type': 'application/json'})
            res.raise_for_status()
            rdict = res.json()
            LOGGER.debug('Added comment %s to bug %s', rdict["result"]["id"], bug)
    return bugs


def comment_already_posted(bug, pipeline_url):
    """Return True if the pipeline results have already been posted to the given bug."""
    pipeline_text = f'Downstream Pipeline: {pipeline_url}'
    comments = bug.getcomments()
    for comment in comments:
        if comment['creator'] == environ['BUGZILLA_EMAIL'] and pipeline_text in comment['text']:
            LOGGER.info('Excluding bug %s as pipeline was already posted in comment %s.', bug.id,
                        comment['count'])
            return True
    return False


def get_pipeline_job_ids(pipeline):
    """Return the list of setup job IDs."""
    jobs = pipeline.jobs.list(all=True)
    job_ids = [job.id for job in jobs if job.stage == 'setup' and job.status == 'success']
    return job_ids


def check_associated_mr(merge_request):
    """Return head pipeline ID or None based on whether we care about the MR."""
    if merge_request.work_in_progress:
        LOGGER.info("MR %s is marked work in progress, ignoring.", merge_request.iid)
        return None
    if merge_request.head_pipeline is None:
        LOGGER.info("MR %s has not triggered any pipelines? head_pipeline is None.",
                    merge_request.iid)
        return None

    pipeline_id = merge_request.head_pipeline.get('id')
    if not merge_request.head_pipeline.get('web_url', '').startswith(f'{defs.GITFORGE}/redhat/'):
        LOGGER.info("MR %s head pipeline #%s is not in the Red Hat namespace: %s",
                    merge_request.iid, pipeline_id, merge_request.head_pipeline.get('web_url'))
        return None

    return pipeline_id


def format_repo_url(checkout_data, debug):
    """Return a repo url string with arch replaced by $basearch."""
    repo_url, arch = next(((build['repo_url'], build['arch']) for build in checkout_data['builds']
                           if build['debug'] is debug), (None, None))
    return repo_url.replace(arch, '$basearch') if repo_url else None


def create_artifacts_text(checkout_data, pipeline_type):
    """Return formatted job data."""
    text = '----------------------------------------\n\n'
    text += (f'Downstream Pipeline Name: {pipeline_type.name}'
             f' ({checkout_data["bridge_job_name"]})\n')
    text += f'Downstream Pipeline: {checkout_data["pipeline_url"]}\n'
    if not checkout_data['public_project']:
        if repo_url := format_repo_url(checkout_data, debug=False):
            text += f'Repo URL: {repo_url}\n'
        if debug_repo_url := format_repo_url(checkout_data, debug=True):
            text += f'Debug Repo URL: {debug_repo_url}\n'

    for build in checkout_data['builds']:
        nvr = f"{checkout_data['version']}.{build['arch']}"
        if build['debug']:
            nvr += '-debug'
        text += '\n'
        text += f"{nvr}:\n"
        text += f"Artifacts (RPMs): {build['browse_url']}\n"
        if checkout_data['public_project']:
            text += f"Repo URL: {build['repo_url']}\n"
    return text


def get_artifact(job, path):
    """Return the given file from the job."""
    try:
        return job.artifact(path)
    except GitlabGetError:
        LOGGER.warning('Error getting path for job #%d: %s', job.id, path)
        return None


def parse_kcidb_files(jobs):
    """Return a dict with the kernel build information."""
    all_kcidb_data = []
    for job in jobs:
        if job_kcidb := get_artifact(job, 'kcidb_all.json'):
            all_kcidb_data.append(json.loads(job_kcidb))

    # Verify we really have expected data for all builds, we don't want to post
    # the links to half of the jobs if artifact retrieval for some of them fails,
    # or if not all jobs were successful. That means, the number of expected
    # builds in the KCIDB data needs to match both the number of jobs and the
    # number of artifacts we actually managed to retrieve.
    if len(jobs) != len(all_kcidb_data) or len(jobs) != len(all_kcidb_data[0]['builds']):
        return {}

    # There is only one checkout with same data in all jobs
    checkout = all_kcidb_data[0]['checkouts'][0]

    # Default to True, if the value is not available it means we're not
    # eligible for any targeted testing
    all_sources_targeted = misc.get_nested_key(checkout, 'misc/all_sources_targeted', True)
    version = misc.get_nested_key(checkout, 'misc/kernel_version')

    # The downstream pipeline URL this job is a part of.
    ds_pipeline_url = next((job.pipeline['web_url'] for job in jobs))

    builds = []
    for kcidb_data in all_kcidb_data:
        for build in kcidb_data['builds']:
            if not build.get('valid'):
                # This is a data placeholder for build plan and we find the data in
                # another kcidb_data element. If the build failed, we wouldn't get here.
                continue

            repo_url = next((output_file['url'] for output_file in build['output_files']
                             if output_file['name'] == 'kernel_package_url'), None)
            browse_url = next((output_file['url'] for output_file in build['output_files']
                               if output_file['name'] == 'kernel_browse_url'), None)
            arch = build['architecture']
            debug = misc.get_nested_key(build, 'misc/debug')

            builds.append({'browse_url': browse_url,
                           'repo_url': repo_url,
                           'debug': debug,
                           'arch': arch})

    return {'version': version,
            'all_sources_targeted': all_sources_targeted,
            'builds': builds,
            'pipeline_url': ds_pipeline_url}


def get_pipeline_setup_job_ids(gl_project, gl_pipeline):
    """Return the list of successful setup job IDs for the given project/pipeline."""
    if not gl_project or not gl_pipeline:
        LOGGER.info('Could not get downstream pipeline for this bridge job.')
        return []

    # If the pipeline was canceled then we have nothing useful to post, a new run will come in.
    if gl_pipeline.status == 'canceled':
        LOGGER.info('Pipeline was canceled; skipping.')
        return []

    # Get the job IDs of the *succesful* setup jobs.
    if not (job_ids := get_pipeline_job_ids(gl_pipeline)):
        LOGGER.info('No successful setup jobs found for downstream pipeline %s in project %s.',
                    gl_pipeline.id, gl_project.name)
    return job_ids


def process_bridge_job(gl_instance, bridge_job):
    """Process the bridge job and return checkout_data."""
    downstream_project = gl_instance.projects.get(bridge_job.downstream_pipeline['project_id'])
    downstream_pipeline = downstream_project.pipelines.get(bridge_job.downstream_pipeline.get('id'))
    if not (ds_job_ids := get_pipeline_setup_job_ids(downstream_project, downstream_pipeline)):
        return {}

    # "Job methods (play, cancel, and so on) are not available on ProjectPipelineJob objects.
    # To use these methods create a ProjectJob object."
    project_jobs = [downstream_project.jobs.get(job_id) for job_id in ds_job_ids]

    if checkout_data := parse_kcidb_files(project_jobs):
        # Wedge the downstream project visibility into the checkout_data so we know how to format
        # the BZ comment.
        checkout_data['public_project'] = downstream_project.visibility == 'public'
        # Wedge the bridge job name into the checkout data.
        checkout_data['bridge_job_name'] = bridge_job.name

    return checkout_data


def filter_bridge_job(bridge_job, pipe_type, branch):
    """Return True if the bridge job should be processed, otherwise False."""
    # Possibly the bridge job does not have a downstream pipeline yet.
    if not bridge_job.downstream_pipeline:
        LOGGER.info('Bridge job %s does not have a downstream pipeline.', bridge_job.name)
        return False
    if pipe_type not in branch.pipelines:
        LOGGER.info("Ignoring bridge job %s because it doesn't get an MR label on branch '%s'.",
                    bridge_job.name, branch.name)
        return False
    return True


def process_bridge_jobs(gl_instance, branch, bridges_list):
    """Process each bridge job in the bridges_list and return a dict of type: checkout_data."""
    jobs = {}
    for bridge_job in bridges_list:
        pipe_type = PipelineType.get(bridge_job.name)
        if not filter_bridge_job(bridge_job, pipe_type, branch):
            continue
        LOGGER.info('Fetching data for %s (%s) #%s', bridge_job.name, pipe_type.name, bridge_job.id)
        if not (checkout_data := process_bridge_job(gl_instance, bridge_job)):
            LOGGER.debug('No checkout data.')
            continue
        jobs[pipe_type] = checkout_data
    # If this Branch has a compat pipeline then we want to avoid posting centos pipeline results
    # but then also only post the compat pipeline results if the centos pipeline has finished. Ha.
    if jobs and PipelineType.RHEL_COMPAT in branch.pipelines:
        if PipelineType.CENTOS not in jobs:
            LOGGER.info(('Ignoring any rhel compat pipeline as there are no centos pipeline '
                         'results at this time.'))
            jobs.pop(PipelineType.RHEL_COMPAT, None)
        LOGGER.info('Ignoring any centos pipeline for a Branch with rhel compat.')
        jobs.pop(PipelineType.CENTOS, None)
    return jobs


def set_targeted_testing_label(gl_project, mr_id, pipe_type, all_sources_targeted):
    """Add or remove the targeted testing label as needed.."""
    if pipe_type not in (PipelineType.RHEL, PipelineType.CENTOS):
        LOGGER.info('No Targeted Testing label for this pipeline.')
        return
    if all_sources_targeted:
        LOGGER.info('Remove Targeted Testing label for this pipeline.')
        common.remove_labels_from_merge_request(gl_project, mr_id,
                                                [defs.TARGETED_TESTING_LABEL])
        return
    LOGGER.info('Add Targeted Testing label for this pipeline.')
    common.add_label_to_merge_request(gl_project, mr_id, [defs.TARGETED_TESTING_LABEL])


def _process_pipeline(gl_project, gl_mr, branch):
    """Process the given pipeline."""
    mr_description = Description(gl_mr.description)

    # If the MR doesn't have any BZs or JIssues listed, then we have nothing to do.
    if not mr_description.bugzilla and not mr_description.jissue:
        LOGGER.info('No bugs nor JIRA issues found in MR description, nothing to do.')
        return
    LOGGER.info('Found BZs %s and JIRA issues %s.', mr_description.bugzilla, mr_description.jissue)

    local_pipeline = gl_project.pipelines.get(gl_mr.head_pipeline.get('id'))
    bzcon = None

    LOGGER.info("Branch '%s' uses pipelines: %s", branch.name, [b.name for b in branch.pipelines])

    # Get data from all jobs.
    bridge_jobs = process_bridge_jobs(gl_project.manager.gitlab, branch,
                                      local_pipeline.bridges.list())

    # Store each pipeline comment so we can post them all at once, maybe.
    comment_dict = {}
    header = HEADER % (gl_mr.title, gl_mr.web_url, local_pipeline.web_url)

    # Process each of the expected bridge jobs separately.
    for pipe_type, checkout_data in bridge_jobs.items():
        # We need a bugzilla connection to be able to just post.
        if mr_description.bugzilla and not bzcon:
            if not (bzcon := common.try_bugzilla_conn()):
                LOGGER.error('No bugzilla connection.')
                return

        comment_dict[pipe_type] = create_artifacts_text(checkout_data, pipe_type)

        if mr_description.bugzilla:
            bugs = post_to_bugs(mr_description.bugzilla, header + comment_dict[pipe_type],
                                checkout_data['pipeline_url'], pipe_type, bzcon)

            # For kernel-rt we separately add external tracker links to those CVE BZs.
            if pipe_type is PipelineType.REALTIME and bugs:
                link_mr_to_bzs(bugs, make_ext_bz_bug_id(gl_project.path_with_namespace, gl_mr.iid),
                               bzcon)

        # For the regular kernel pipeline we may add a label if there are not any targeted tests.
        set_targeted_testing_label(gl_project, gl_mr.iid, pipe_type,
                                   checkout_data.get('all_sources_targeted'))

    # A single post for jira.
    if mr_description.jissue:
        # Sort the dict so the pipeline artifacts comment will be in a consistent order
        mr_issues = make_jissues(mr_description.jissue, mrs=[])
        comment_text = header + '\n'.join(dict(sorted(comment_dict.items())).values())
        pipeline_urls = [checkout['pipeline_url'] for checkout in bridge_jobs.values()]
        update_testable_builds(mr_issues, comment_text, pipeline_urls)


def process_pipeline_event(msg, projects, **_):
    """Parse pipeline or merge_request msg and run _process_pipeline."""
    if msg.payload['object_kind'] == 'pipeline':
        # Some pipeline events are not associated with an MR. Ignore them.
        if msg.payload['merge_request'] is None:
            LOGGER.info('Pipeline %s is not associated with an MR, ignoring.',
                        msg.payload['object_attributes']['id'])
            return
        mr_id = msg.payload["merge_request"]["iid"]
    elif msg.payload['object_kind'] == 'merge_request':
        mr_id = msg.payload["object_attributes"]["iid"]

    gl_instance = get_instance(defs.GITFORGE)
    gl_project = gl_instance.projects.get(msg.payload["project"]["path_with_namespace"])
    if not (gl_mr := common.get_mr(gl_project, mr_id)):
        return

    branch = projects.get_target_branch(gl_mr.project_id, gl_mr.target_branch)

    if check_associated_mr(gl_mr):
        _process_pipeline(gl_project, gl_mr, branch)


def process_job_event(msg, projects, **_):
    """Process a job event from a downstream pipeline.

    We aim to update the BZs on successful setup jobs, before the testing finishes.
    """
    # Quickly filter out events we don't care about
    if msg.payload['build_status'] != 'success' or msg.payload['build_stage'] != 'setup':
        LOGGER.info("Ignoring event for pipeline %s with stage '%s' and status '%s'.",
                    msg.payload['pipeline_id'], msg.payload['build_stage'],
                    msg.payload['build_status'])
        return

    gl_instance = get_instance(defs.GITFORGE)
    downstream_project = gl_instance.projects.get(msg.payload['project_id'])
    downstream_pipeline = downstream_project.pipelines.get(msg.payload['pipeline_id'])
    mr_url = get_variables(downstream_pipeline).get('mr_url')
    if not mr_url:   # No MR associated with the job/pipeline
        return

    _, merge_request = parse_gitlab_url(mr_url)
    mr_project = gl_instance.projects.get(merge_request.project_id)

    branch = projects.get_target_branch(merge_request.project_id, merge_request.target_branch)

    if check_associated_mr(merge_request):
        _process_pipeline(mr_project, merge_request, branch)


def process_mr_event(msg, projects, **_):
    """Process a merge request event message."""
    namespace = msg.payload['project']['path_with_namespace']
    mr_id = msg.payload["object_attributes"]["iid"]
    action = msg.payload['object_attributes'].get('action')
    LOGGER.info('Processing event for MR %s!%s (%s)', namespace, mr_id, action)

    mr_desc = msg.payload['object_attributes'].get('description', None)
    if mr_desc is None:
        gl_instance = msg.gl_instance()
        project = gl_instance.projects.get(namespace)
        merge_request = common.get_mr(project, mr_id)
        mr_desc = merge_request.description if merge_request else None

    is_draft, draft_changed = common.draft_status(msg.payload)
    mr_desc_changed = 'description' in msg.payload.get('changes')

    # If the MR is a Draft we want to ignore it unless it just flipped to Draft state then
    # trick bugs_to_process() into giving us all the bugs to unlink.
    if is_draft:
        if draft_changed:
            LOGGER.info('MR changed to Draft, unlinking bugs.')
            action = 'close'
        else:
            LOGGER.info('MR is a Draft, ignoring.')
            return

    bz_jira_changes = common.bugs_to_process(mr_desc, action, msg.payload['changes'])
    if not any(bz_jira_changes.values()):
        LOGGER.info('No relevant bugs or jissues found in MR description.')
        return
    LOGGER.info('Found changes: %s', bz_jira_changes)

    # Has the Draft flag been removed? Or MR description changed? Then run the pipeline linker.
    if ((bz_jira_changes['link'] or bz_jira_changes['link_jissue']) and not is_draft) and \
            (draft_changed or mr_desc_changed):
        LOGGER.info('Sending MR event to pipeline processor.')
        process_pipeline_event(msg, projects)

    # If the event is not for a new or closing MR and does not correspond to some change in the MR
    # description or draft status then we can ignore it.
    if action not in ('open', 'close', 'reopen') and not draft_changed and not mr_desc_changed:
        LOGGER.info('MR has not changed, ignoring event.')
        return

    if bz_jira_changes['unlink_jissue'] and action == 'close':
        remove_gitlab_link_in_issues(mr_id, namespace, bz_jira_changes['unlink_jissue'])

    if not (bzcon := common.try_bugzilla_conn()):
        return

    update_bugzilla(bz_jira_changes, namespace, mr_id, bzcon)


WEBHOOKS = {
    "merge_request": process_mr_event,
    'pipeline': process_pipeline_event,
    'build': process_job_event
}


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('BUGLINKER')
    args = parser.parse_args(args)
    common.generic_loop(args, WEBHOOKS, get_gl_instance=False)


if __name__ == "__main__":
    main(sys.argv[1:])
