"""Webhook interaction tests."""
from copy import deepcopy
from unittest import TestCase
from unittest import mock

from tests import fake_payloads
from tests import fakes
from tests import test_base_mr
from webhook import signoff
from webhook.defs import DCOState
from webhook.description import Commit


@mock.patch.dict('os.environ', {'RH_METADATA_YAML_PATH': 'tests/fake_rh_metadata.yaml'})
class TestMR(TestCase):
    """Tests for the signoff MR dataclass."""

    @mock.patch.object(signoff.MR, 'check_signoffs', mock.Mock())
    def test_mr_init(self):
        """Returns an MR object."""
        graphql = mock.Mock()
        graphql.client.query.side_effect = [test_base_mr.MR1_COMMITS_RESULT,
                                            test_base_mr.MR1_QUERY_RESULT]
        graphql.get_mr_descriptions.return_value = test_base_mr.MR1_DESCRIPTION_QUERY_RESULT
        test_mr = test_base_mr.create_mr(mr_func=signoff.MR, graphql=graphql)
        test_mr.check_signoffs.assert_called_once()
        self.assertEqual(len(test_mr.commits), 2)

    def test_check_signoffs(self):
        """Looks up signoff emails for NAME_MATCHES commits and tries to link them to the author."""
        author = deepcopy(test_base_mr.AUTHOR1)
        author['email'] = 'author_email@redhat.com'
        new_commit = {'authorEmail': 'another_email@redhat.com',
                      'authorName': author['name'],
                      'author': author,
                      'authoredDate': '2022-12-19T08:58:26+01:00',
                      'description': 'Bugzilla: https://bugzilla.redhat.com/12345\n'
                                     f"Signed-off-by: {author['name']}"
                                     ' <author_alias@redhat.com>',
                      'sha': 'aeefde19ed4139516342fc07624429da45e7e12b',
                      'title': 'mr1 head commit'}
        mr_commits = deepcopy(test_base_mr.MR1_COMMITS_RESULT)
        mr_commits['project']['mr']['commits']['nodes'].insert(0, new_commit)
        mock_graphql = mock.Mock()
        mock_graphql.find_member_by_email.return_value = author
        mock_graphql.client.query.side_effect = [mr_commits, test_base_mr.MR1_QUERY_RESULT]
        mock_graphql.get_mr_descriptions.return_value = {}
        test_mr = test_base_mr.create_mr(mr_func=signoff.MR, graphql=mock_graphql)
        mock_graphql.find_member_by_email.assert_called_once()
        user = test_mr.user_cache.data[author['username']]
        commit = test_mr.commits[new_commit['sha']]
        self.assertIn('another_email@redhat.com', user.emails)
        self.assertIs(commit.dco_state, DCOState.OK)


class TestHelpers(TestCase):
    """Tests for the various helper functions."""

    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.common.update_webhook_comment')
    def test_update_mr(self, mock_update_comment, mock_add_label):
        """Updates the comment and possibly sets a new label."""
        namespace = 'goup/project'
        mr_id = 123
        username = 'mock_user'
        mock_instance = fakes.FakeGitLab()
        mock_instance.user = mock.Mock(username=username)
        mock_project = mock_instance.add_project(321, namespace)
        mock_mr = mock_project.add_mr(mr_id)

        # Scope changed, calls add_label.
        mock_mr.labels = ['Signoff::OK']
        signoff.update_mr(mock_project, mr_id, 'text')
        mock_add_label.assert_called_once_with(mock_project, mr_id, ['Signoff::NeedsReview'])
        expected_text = signoff.REPORT_HEADER + ' ~"Signoff::NeedsReview"\n\ntext'
        mock_update_comment.assert_called_once_with(mock_mr, mock_instance.user.username,
                                                    signoff.REPORT_HEADER, expected_text)

        # Scope hasn't changed, no add_label.
        mock_add_label.reset_mock()
        mock_update_comment.reset_mock()
        mock_mr.labels = ['Signoff::OK']
        text = signoff.REPORT_STATUS % 'PASSED'
        signoff.update_mr(mock_project, mr_id, text)
        mock_add_label.assert_not_called()
        expected_text = signoff.REPORT_HEADER + ' ~"Signoff::OK"\n\n' + text
        mock_update_comment.assert_called_once_with(mock_mr, mock_instance.user.username,
                                                    signoff.REPORT_HEADER, expected_text)

        # No existing label.
        mock_add_label.reset_mock()
        mock_update_comment.reset_mock()
        mock_mr.labels = []
        text = signoff.REPORT_STATUS % 'PASSED'
        signoff.update_mr(mock_project, mr_id, text)
        mock_add_label.assert_called_once_with(mock_project, mr_id, ['Signoff::OK'])
        expected_text = signoff.REPORT_HEADER + ' ~"Signoff::OK"\n\n' + text
        mock_update_comment.assert_called_once_with(mock_mr, mock_instance.user.username,
                                                    signoff.REPORT_HEADER, expected_text)

    def test_generate_report(self):
        """Returns a markdown string of results."""
        # "Bad" results.
        commits = [Commit(input_dict=raw_commit) for raw_commit in test_base_mr.COMMITS]
        result = signoff.generate_report(commits)
        self.assertIn(signoff.REPORT_STATUS % 'FAILED', result)
        self.assertIn(signoff.REPORT_HELP, result)
        self.assertIn(signoff.REPORT_FOOTER, result)

        # "Good" results.
        commits = [mock.Mock(dco_state=DCOState.OK)]
        result = signoff.generate_report(commits)
        self.assertIn(signoff.REPORT_STATUS % 'PASSED', result)

        # "Bad" results due to no commits.
        commits = []
        result = signoff.generate_report(commits)
        self.assertIn(signoff.REPORT_STATUS % 'FAILED', result)
        self.assertIn(signoff.REPORT_NO_COMMITS, result)

    @mock.patch('webhook.signoff.MR')
    @mock.patch('webhook.signoff.generate_report')
    @mock.patch('webhook.signoff.update_mr')
    def test_process_mr(self, mock_update, mock_report, mock_mr):
        """Creates the MR object and updates the MR."""
        mock_graphql = mock.Mock()
        mock_instance = mock.Mock()
        mock_projects = mock.Mock()
        mr_url = 'https://gitlab.com/group/project/-/merge_requests/123'
        signoff.process_mr(mock_graphql, mock_instance, mock_projects, mr_url)
        mock_mr.assert_called_once()
        mock_report.assert_called_once_with(mock_mr.return_value.all_commits)
        mock_update.assert_called_once()


class TestEventHandlers(TestCase):
    """Tests for the event handlers."""

    @staticmethod
    @mock.patch('webhook.signoff.process_mr')
    def run_process_event(event_func, payload, process_mr_called, mock_process_mr):
        """Pass a payload to the event_func and assert if process_mr is called."""
        mock_instance = mock.Mock()
        mock_msg = mock.Mock(payload=payload)
        mock_graphql = mock.Mock()
        mock_projects = mock.Mock()
        event_func(mock_instance, mock_msg, mock_graphql, mock_projects)
        if process_mr_called:
            mock_process_mr.assert_called()
        else:
            mock_process_mr.assert_not_called()

    def test_process_mr_event_no_change(self):
        """Does not call process_mr because there is no relevant change."""
        payload = deepcopy(fake_payloads.MR_PAYLOAD)
        self.run_process_event(signoff.process_mr_event, payload, False)

    def test_process_mr_event_action_affects_commits(self):
        """Calls process_mr because mr_action_affects_commits."""
        payload = deepcopy(fake_payloads.MR_PAYLOAD)
        payload['object_attributes']['oldrev'] = 'ahash'
        self.run_process_event(signoff.process_mr_event, payload, True)

    def test_process_mr_event_description_changed(self):
        """Calls process_mr because the MR description changed."""
        payload = deepcopy(fake_payloads.MR_PAYLOAD)
        payload['changes']['description'] = {'previous': 'huh', 'current': 'woah'}
        self.run_process_event(signoff.process_mr_event, payload, True)

    def test_process_mr_event_label_changed(self):
        """Calls process_mr because the Signoff label changed."""
        payload = deepcopy(fake_payloads.MR_PAYLOAD)
        payload['changes']['labels'] = {'previous': [{'title': 'Signoff::OK'}],
                                        'current': [{'title': 'Signoff::NeedsReview'}]}
        self.run_process_event(signoff.process_mr_event, payload, True)

    def test_process_note_event_no_change(self):
        """Does not call process_mr because there is no relevant change."""
        payload = deepcopy(fake_payloads.NOTE_PAYLOAD)
        self.run_process_event(signoff.process_note_event, payload, False)

    def test_process_note_event_requests_evaluation(self):
        """Calls process_mr because evaluation was requested."""
        payload = deepcopy(fake_payloads.NOTE_PAYLOAD)
        payload['object_attributes']['note'] = 'request-signoff-evaluation'
        self.run_process_event(signoff.process_note_event, payload, True)
        payload['object_attributes']['note'] = 'request-dco-evaluation'
        self.run_process_event(signoff.process_note_event, payload, True)
